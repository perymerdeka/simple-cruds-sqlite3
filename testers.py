import sqlite3

def tambah_data(id, nama, email):
    try:
        sqliteConnection = sqlite3.connect('database_siswa.db')
        cursor = sqliteConnection.cursor()
        print("Berhasil Terkoneksi ke Database")

        sqlite_insert_with_param = """INSERT INTO data_siswa
                          (id, nama, email) 
                          VALUES (?, ?, ?);"""

        data_tuple = (id, nama, email)
        cursor.execute(sqlite_insert_with_param, data_tuple)
        sqliteConnection.commit()
        print("berhasil menambah data ke tabel")

        cursor.close()

    except sqlite3.Error as error:
        print("Error Gagal Terkoneksi ke Database", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("Koneksi SQLite selesai")

# tambah_data(2, 'pery', 'pery@pesonainformatika.com')
# tambah_data(3, 'mimin', 'mimin@pesonainformatika.com')

# Fungsi untuk membaca Data
def lihat_data():
    try:
        sqliteConnection = sqlite3.connect('database_siswa.db')
        cursor = sqliteConnection.cursor()
        print("Berhasil Terkoneksi Ke Database")

        sqlite_select_query = """SELECT * from data_siswa"""
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        print("Total Baris:  ", len(records))
        for row in records:
            print("Id: ", row[0])
            print("Nama: ", row[1]) 
            print("Email: ", row[2])
            print("\n")

        cursor.close()

    except sqlite3.Error as error:
        print("Gagal Membaca Data dari Tabel", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("Koneksi SQLite Selesai")

 
# Melihat Data

# lihat_data()


# fungsi untuk mencari data
def get_data_siswa(id):
    try:
        sqliteConnection = sqlite3.connect('database_siswa.db')
        cursor = sqliteConnection.cursor()
        print("Berhasil terkoneksi ke database")

        sql_select_query = """select * from data_siswa where id = ?"""
        cursor.execute(sql_select_query, (id,))
        records = cursor.fetchall()
        print(" ID yang di masukan ", id)
        for row in records:
            print("Nama = ", row[1])
            print("Email  = ", row[2])
        cursor.close()

    except sqlite3.Error as error:
        print("Gagal Terkoneksi ke SQLite", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("Koneksi SQLite Selesai")

# mencari siswa
# get_data_siswa(2)

# fungsi untuk update data dari database
def update_data_siswa(id, nama):
    try:
        sqliteConnection = sqlite3.connect('database_siswa.db')
        cursor = sqliteConnection.cursor()
        print("Berhasil terkoneksi ke database")

        sql_update_query = """Update data_siswa set nama = ? where id = ?"""
        data = (nama, id)
        cursor.execute(sql_update_query, data)
        sqliteConnection.commit()
        print("Update Data Sukses")
        cursor.close()

    except sqlite3.Error as error:
        print("gagal terkoneksi ke tabel", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("koneksi SQLite Selesai")
   
# update Data
# update_data_siswa(2, 'budiman')


# Fungsi Untuk hapus Data
def hapus_data_siswa(id):
    try:
        sqliteConnection = sqlite3.connect('database_siswa.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sql_update_query = """DELETE from data_siswa where id = ?"""
        cursor.execute(sql_update_query, (id, ))
        sqliteConnection.commit()
        print("Hapus Data Sukses")

        cursor.close()

    except sqlite3.Error as error:
        print("gagal terkoneksi ke tabel", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("koneksi SQLite Selesai")

# hapus_data
hapus_data_siswa(2)


